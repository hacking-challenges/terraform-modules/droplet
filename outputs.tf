output "id" {
  value = digitalocean_droplet.this.id
}

output "ip" {
  value = digitalocean_droplet.this.ipv4_address
}
